package lesson50homework.microgram.Model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;

@Document
public class Like {
    private static final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
    private static final Random r = new Random();

    @Id
    private String id;

    @DBRef
    private User user;
    @DBRef
    private Publication publication;
    private LocalDateTime timeOfLike;

    public Like(User user, Publication publication) {
        this.user = user;
        this.publication = publication;
        timeOfLike = LocalDateTime.now();
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Publication getPublication() {
        return publication;
    }

    public void setPublication(Publication publication) {
        this.publication = publication;
    }

    private static String generateId() {
        return LocalDateTime.now().format(dtf) + r.nextInt();
    }

    public LocalDateTime getTimeOfLike() {
        return timeOfLike;
    }

    public void setTimeOfLike(LocalDateTime timeOfLike) {
        this.timeOfLike = timeOfLike;
    }
}