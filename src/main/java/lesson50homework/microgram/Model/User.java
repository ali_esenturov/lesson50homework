package lesson50homework.microgram.Model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.sql.SQLOutput;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


@Document
public class User {
    private static final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
    private static final Random r = new Random();

    @Id
    private String id = generateId();

    private String login;
    private String email;
    private String password;
    private List<Publication> publications = new ArrayList<>();
    @DBRef
    private List<User> followers = new ArrayList<>();
    @DBRef
    private List<User> subscriptions = new ArrayList<>();
    private int countPublications;
    private int countSubscriptions;
    private int countFollowers;


    public User(String login, String email, String password) {
        this.login = login;
        this.email = email;
        this.password = password;
    }

    @Override
    public String toString() {
        return login;
    }

    public void printUser(){
        for(Publication p: publications){
            System.out.println(p);
        }
    }

    public List<Publication> getPublications() {
        return publications;
    }

    public void setPublications(List<Publication> publications) {
        this.publications = publications;
    }

    public List<User> getFollowers() {
        return followers;
    }

    public List<User> getSubscriptions() {
        return subscriptions;
    }

    private static String generateId() {
        return LocalDateTime.now().format(dtf) + r.nextInt();
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getCountPublications() {
        return countPublications;
    }

    public void setCountPublications(int countPublications) {
        this.countPublications = countPublications;
    }

    public int getCountSubscriptions() {
        return countSubscriptions;
    }

    public void setCountSubscriptions(int countSubscriptions) {
        this.countSubscriptions = countSubscriptions;
    }

    public int getCountFollowers() {
        return countFollowers;
    }

    public void setCountFollowers(int countFollowers) {
        this.countFollowers = countFollowers;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
