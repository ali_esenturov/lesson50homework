package lesson50homework.microgram.Model;

import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User,String> {
    User getUserByLogin(String login);
}