package lesson50homework.microgram.Model;

import org.springframework.data.repository.CrudRepository;

public interface SubscriptionRepository extends CrudRepository<User,String> {
}