package lesson50homework.microgram.Model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Document
public class Publication {
    private static final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
    private static final Random r = new Random();

    @Id
    private String id = generateId();

    @DBRef
    private User user;
    private String img;
    private String description;
    private LocalDateTime timeOfPublication;
    @DBRef
    private List<Comment> comments = new ArrayList<>();


    public Publication(String img, String description) {
        this.img = img;
        this.description = description;
        timeOfPublication = LocalDateTime.now();
    }

    @Override
    public String toString() {
        return id;
    }

    private static String generateId() {
        return LocalDateTime.now().format(dtf) + r.nextInt();
    }

    public List<Comment> getComments() {
        return comments;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getTimeOfPublication() {
        return timeOfPublication;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setTimeOfPublication(LocalDateTime timeOfPublication) {
        this.timeOfPublication = timeOfPublication;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }
}
