package lesson50homework.microgram.util;

import lesson50homework.microgram.Model.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Configuration
public class DatabasePreloader {
    private static final Random r = new Random();

    @Bean
    CommandLineRunner initDatabase(UserRepository userRepository, PublicationRepository publicationRepository,
                                   LikeRepository likeRepository, CommentRepository commentRepository) {
            userRepository.deleteAll();
            publicationRepository.deleteAll();
            likeRepository.deleteAll();
            commentRepository.deleteAll();
            return(args) -> save(userRepository, publicationRepository, commentRepository);
    }

    private void save(UserRepository userRepository, PublicationRepository publicationRepository,
                      CommentRepository commentRepository){

        List<User> users = Arrays.asList(users());
        users.forEach(userRepository::save);                        //adding users

        List<Publication> publications = Arrays.asList(publications());
        for(Publication p : publications){
            int user_index = r.nextInt(users.size());
            User user = users.get(user_index);
            p.setUser(user);

            String userLogin = user.getLogin();

            User u = userRepository.getUserByLogin(userLogin);
            u.getPublications().add(p);

            System.out.println(u);
            u.printUser();
            System.out.println("-----------------------------");

            userRepository.save(u);
        }
        publications.forEach(publicationRepository::save);

        fillComments(commentRepository, userRepository, publicationRepository);
    }

    private void fillComments(CommentRepository commentRepository, UserRepository userRepository,
                              PublicationRepository publicationRepository){

        int commentsAmount = r.nextInt(10) + 5;

        Iterable<User> foundUsers = userRepository.findAll();
        List<User> users = StreamSupport
                    .stream(foundUsers.spliterator(), false)
                    .collect(Collectors.toList());

        Iterable<Publication> foundPublications = publicationRepository.findAll();
        List<Publication> publications = StreamSupport
                .stream(foundPublications.spliterator(), false)
                .collect(Collectors.toList());

        for(int i = 0; i < commentsAmount; i++){
            int randomUser = r.nextInt(users.size());
            int randomPublication = r.nextInt(publications.size());

            Comment comment = new Comment(users.get(randomUser), Generator.makeDescription());
            publications.get(randomPublication).getComments().add(comment);
            commentRepository.save(comment);
        }
        publicationRepository.saveAll(publications);
    }

    private User[] users() {
        return new User[]{
                new User("Markus Sillman", "1@gmail.com","1"),
                new User("Nikita Culler", "2@gmail.com", "2"),
                new User("Tawanna Melanson", "3@gmail.com", "3"),
                new User("Brunilda Mikels", "4@gmail.com", "4"),
                new User("Hubert Takahashi", "5@gmail.com", "5"),
                new User("Hershel Caffrey", "6@gmail.com", "6")
        };
    }

    private Publication[] publications() {
        return new Publication[]{
                new Publication("1.jpg", "1"),
                new Publication("2.jpg", "2"),
                new Publication("3.jpg", "3"),
                new Publication("4.jpg", "4"),
                new Publication("5.jpg", "5"),
                new Publication("6.jpg", "6")
        };
    }
}
