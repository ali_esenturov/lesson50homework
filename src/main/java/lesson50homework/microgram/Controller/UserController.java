package lesson50homework.microgram.Controller;

import lesson50homework.microgram.Model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class UserController {
    @Autowired
    LikeRepository likeRepository;
    @Autowired
    PublicationRepository publicationRepository;
    @Autowired
    UserRepository userRepository;

    @GetMapping("/addUser")
    @ResponseStatus(HttpStatus.SEE_OTHER)
    public String saveUser() {
        userRepository.deleteAll();
        publicationRepository.deleteAll();
        likeRepository.deleteAll();

        User example_user = new User("1", "login", "nurs@mail.ru");
        userRepository.save(example_user);
        return "redirect:/answer";
    }

    @GetMapping("/answer")
    public String getAnswer(Model model) {
        model.addAttribute("name", "Success");
        return "answer";
    }
}
